DECLARE @StartDate  date = '20000101';
DECLARE @CutoffDate date = DATEADD(DAY, -1, DATEADD(YEAR, 100, @StartDate));

;WITH seq(n) AS 
(
  SELECT 0 UNION ALL SELECT n + 1 FROM seq
  WHERE n < DATEDIFF(DAY, @StartDate, @CutoffDate)
),
d(d) AS 
(
  SELECT DATEADD(DAY, n, @StartDate) FROM seq
),
src AS
(
  SELECT
    DateId          = CONVERT(date, d),
    DayInt          = DATEPART(DAY,       d),
    [DayName]       = DATENAME(WEEKDAY,   d),
    WeekId          = DATEPART(WEEK,      d),
    MonthId         = CONVERT(datetime, d),
	  DayId           = CONVERT(datetime, d),
    MonthInt        = DATEPART(MONTH,     d),
    [MonthName]     = DATENAME(MONTH,     d),
    QuarterId       = DATEPART(Quarter,   d),
    YearInt         = DATEPART(YEAR,      d),
    YearId          = CONVERT(datetime, d),
	  FirstOfMonth    = DATEFROMPARTS(YEAR(d), MONTH(d), 1),
	  LastOfMonth     = CONVERT(date, DATEADD(MM,DATEDIFF(MM, -1, d),-1)),
    TheLastOfYear   = DATEFROMPARTS(YEAR(d), 12, 31),
    TheDayOfYear    = DATEPART(DAYOFYEAR, d)
  FROM d
),
CTE AS (
SELECT NUMBER FROM master.dbo.spt_values where TYPE='P' AND NUMBER BETWEEN 0 AND 24
)
,Hours_CTE AS (
    SELECT NUMBER AS H FROM CTE WHERE NUMBER BETWEEN 0 AND 23
)
,RawTable as
(
SELECT*
	FROM src
CROSS APPLY 
(
SELECT 
	DATEADD(hour,H,CONVERT(datetime, src.DateId)) as [HourId],
	H as [HourInt]
	
	FROM Hours_CTE
) as dsf)


INSERT INTO [dbo].[DimDate]
(
	[DateId],
    [Name],
    [Code],
    [HourId],
    [HourInt],
    [MonthId],
    [MonthInt],
    [MonthName],
    [FirstDayOfMonth],
    [LastDayOfMonth],
    [DayId],
    [DayInt],
    [DayName],
    [QuarterId],
    [WeekId],
    [YearId],
    [YearInt],
    [IsActive]
)
SELECT 
	DateId,
	NULL,
	NULL,
	[HourId],
	[HourInt],
	[MonthId],
	[MonthInt],
	[MonthName],
	FirstOfMonth,
	LastOfMonth,
	DayId,
	DayInt,
	[DayName],
	QuarterId,
	WeekId,
	YearId,
	YearInt,
	1 as [isActive]
	
FROM RawTable
OPTION (MAXRECURSION 0)



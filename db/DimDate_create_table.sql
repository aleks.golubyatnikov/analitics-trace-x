USE [AnalytixAsfarmaDW]
GO

/****** Object:  Table [dbo].[DimDate]    Script Date: 26.09.2022 17:19:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DimDate](
	[Id] [int] IDENTITY(300000,1) NOT NULL,
	[DateId] [date] NULL,
	[Name] [varchar](8000) NULL,
	[Code] [varchar](50) NULL,
	[HourId] [datetime] NULL,
	[HourInt] [smallint] NULL,
	[MonthId] [datetime] NULL,
	[MonthInt] [smallint] NULL,
	[MonthName] [varchar](24) NULL,
	[FirstDayOfMonth] [date] NULL,
	[LastDayOfMonth] [date] NULL,
	[DayId] [datetime] NULL,
	[DayInt] [smallint] NULL,
	[DayName] [varchar](24) NULL,
	[QuarterId] [smallint] NULL,
	[WeekId] [smallint] NULL,
	[YearId] [datetime] NULL,
	[YearInt] [smallint] NULL,
	[IsActive] [varchar](1) NULL,
 CONSTRAINT [pk_DWH_DIMDimDateId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO


